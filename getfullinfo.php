<?php

   // Requires connection to the database through the dbconnection file

   require ('dbconnect.php');

   // Variables from the database

   $id = $_GET['id'];


   // SQL query for gathering key information from database (full stock information)

   $sql2 = "SELECT companyname, currentprice, recentchange, annualtrend, recentchangedirection FROM stocks WHERE id = $id";
   $result2 = $conn->query($sql2);
   
   // SQL query for echoing key information from database (full stock information) as a table

   echo "<table><tr><th>Company Name</th><th>Current Price</th><th>Recent Change</th><th>Annual Trend</th><th>Recent Change Direction</th></tr>";
   while($row = $result2->fetch_assoc()) {
      echo "<tr>";
      echo "<td>" .$row['companyname']. "</td>";
      echo "<td>" .$row['currentprice']. "</td>";
      echo "<td>" .$row['recentchange']. "</td>";
      echo "<td>" .$row['annualtrend']. "</td>";
      echo "<td>" .$row['recentchangedirection']. "</td>";
      echo "</tr>";
      } 
      echo "</table>";


      
	  ?>
	  