-- phpMyAdmin SQL Dump
-- version 4.4.10
-- http://www.phpmyadmin.net
--
-- Host: localhost:8889
-- Generation Time: Jan 28, 2016 at 02:27 AM
-- Server version: 5.5.42
-- PHP Version: 5.6.10

--
-- Database: `COMP333`
--

-- --------------------------------------------------------

--
-- Table structure for table `stocks`
--

CREATE TABLE `stocks` (
  `companyname` varchar(30) DEFAULT NULL,
  `id` int(11) NOT NULL,
  `currentprice` decimal(11,2) NOT NULL,
  `recentchange` decimal(11,2) NOT NULL,
  `annualtrend` varchar(5) NOT NULL,
  `recentchangedirection` varchar(5) NOT NULL
);

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
 `username` varchar(30) NOT NULL,
 `id` int(11) NOT NULL,
 `password` varchar(30) NOT NULL 
);

--
-- Indexes for table `stocks`
--

ALTER TABLE `stocks`
  ADD PRIMARY KEY (`id`);
  
--
-- Indexes for table `users`
--

ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Table structure for table `userstocks`
--

CREATE TABLE `userstocks` (
  `uid` int(11) NOT NULL,
  `sid` int(11) NOT NULL,
  FOREIGN KEY (uid) REFERENCES users (`id`),
  FOREIGN KEY (sid) REFERENCES stocks (`id`)
);

--
-- Dumping data for table `stocks`
--

INSERT INTO `stocks` (`companyname`, `id`, `currentprice`, `recentchange`, `annualtrend`, `recentchangedirection`) VALUES
('ABC Company', 1, '0.40', '0.02', 'Up', 'Up'),
('XYZ Logistics', 2, '1.00', '0.05', 'Down', 'Down'),
('Acme Publishing', 3, '1.33', '0.08', 'Up', 'Down'),
('Fling Fing', 4, '0.94', '0.11', 'Down', 'Up'),
('Neutral Networks', 5, '1.25', '0.40', 'Up', 'Up'),
('Total Solutions Inc', 6, '0.55', '0.01', 'Down', 'Up');

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`username`, `id`, `password`) VALUES
('test', 1, 'test'),
('SueH', 2, 'sue'),
('SimonP', 3, 'simon'),
('RylieM', 4, 'rylie'),
('WillS', 5, 'will'),
('LiamM', 6, 'liam');

--
-- Dumping data for table `userstocks`
--

INSERT INTO `userstocks` (`uid`, `sid`) VALUES
(1,5),
(1,2),
(1,1),
(1,3),
(3,6),
(5,4),
(3,2),
(3,1),
(3,4),
(5,5),
(5,2),
(5,3),
(2,4),
(2,5),
(2,2),
(2,1),
(4,4),
(4,5),
(4,2),
(4,6),
(6,4),
(6,3),
(6,1),
(6,2);




--
-- AUTO_INCREMENT for table `stocks`
--
ALTER TABLE `stocks`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--

ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;

-- DROP TABLES
-- DROP TABLE userstocks;
-- DROP TABLE users;
-- DROP TABLE stocks;