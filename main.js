//XHR Object variable / AJAX request / response
function ajaxRequest(url, method, data, callback){

	let request = new XMLHttpRequest();
	
	request.open(method, url, true);
	
	if(method == "POST"){
		request.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	}
	
	request.onreadystatechange = function() {
		if(request.readyState == 4){
			if(request.status == 200){
				response = request.responseText;
				callback(response);
			}
			else{
				handleError(request.statusText);
			}
		}
	};
		
	request.send(data);	
}

// function to check login confirms password matches the username and password entered

function checklogin(){

	let username = document.getElementById("username").value;
	let password = document.getElementById("password").value;
	
	if(username!= ""&&password!=""){
	url = "checklogin.php?username=" + username + "&password=" + password;
	ajaxRequest(url,"GET", "", getstock);
	}
	else {
		alert ("Empty Fields")
	}

}

// function to get information regarding stock from the database

function getstock(response){
	let username = document.getElementById("username").value;
	if(response === "-99"){
		alert("Incorrect Information");
	}
	else{
		let url = "getstock.php?id=" + response + "&username=" + username;
		ajaxRequest(url, "GET","", displaystock)
		}
}

function handleError(errorText){
	alert("An error occurred " + errorText);
}

// function to display the data from function getstock(response) and hide the login

function displaystock(response){
	let input = document.getElementById("login_form");
	let display = document.getElementById("stock_list");
	
	input.style.display = "none";
	display.innerHTML = response;
}

// function to pull the full information of the stock from database

function fullinfo(id){
	let url = "getfullinfo.php?id="+ id;
	ajaxRequest(url, "GET","", displayfullstock)
}

//function to display the full information to the required div element

function displayfullstock() {
	let display = document.getElementById("fullinfo");
	display.innerHTML = response;
}

// function to delete the stock from the database 

function delstock(id,uid){
	let url = "removestock.php?sid="+ id +"&uid=" + uid;
	ajaxRequest(url, "GET","", displaych)
}

// function to update the page after a delete - Asynchronous 

function displaych(){
	checklogin();
	displayfullstock();
	displayadd(response);
}

// function to get information to add stock

function addinfo(uid){
	let url = "addstock.php?uid="+ uid;
	ajaxRequest(url, "GET","", displayadd)
} 

// function to display add stock information

function displayadd(response){
	let display = document.getElementById("addmore")
	display.innerHTML = response;
}

// function to add stock

function addstock(uid,id){
	let url = "addbox.php?uid="+ uid +"&id=" + id;
	ajaxRequest(url, "GET","", displaych)
}


// function to logout

function logout(){
	window.location = 'home.html';
}



